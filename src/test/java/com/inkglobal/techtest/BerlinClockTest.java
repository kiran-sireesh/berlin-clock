package com.inkglobal.techtest;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


public class BerlinClockTest {


    @Test
    public void secondsLampIsLitYellowAndRestOffAtStartOfDay() {
        BerlinClock berlinClock = new BerlinClock("00:00:00");
        assertEquals("Y\nOOOO\nOOOO\nOOOOOOOOOOO\nOOOO", berlinClock.displayTime());
    }

    @Test
    public void secondsLampIsLitYellowOnlyAlternateSeconds() {
        BerlinClock clockWithEvenSecondsTime = new BerlinClock("00:00:00");
        assertEquals("Y", clockWithEvenSecondsTime.displaySeconds());

        clockWithEvenSecondsTime = new BerlinClock("00:00:58");
        assertEquals("Y", clockWithEvenSecondsTime.displaySeconds());


        BerlinClock clockWithOddSecondsTime = new BerlinClock("00:00:01");
        assertEquals("O", clockWithOddSecondsTime.displaySeconds());

        clockWithOddSecondsTime = new BerlinClock("00:00:27");
        assertEquals("O", clockWithOddSecondsTime.displaySeconds());
    }

    @Test
    public void fiveHourLampsAreLitRedForTimeGreaterThanFiveHours() {
        BerlinClock fiveAM = new BerlinClock("05:00:00");
        assertThat(fiveAM.displayHours(), startsWith("ROOO"));

        BerlinClock twoPM = new BerlinClock("14:00:00");
        assertThat(twoPM.displayHours(), startsWith("RROO"));

        BerlinClock elevenPM = new BerlinClock("23:00:00");
        assertThat(elevenPM.displayHours(), startsWith("RRRR"));
    }

    @Test
    public void allHourLampsAreLitWell() {
        BerlinClock fourAM = new BerlinClock("04:00:00");
        assertEquals("OOOO\nRRRR", fourAM.displayHours());

        BerlinClock sixPM = new BerlinClock("06:00:00");
        assertEquals("ROOO\nROOO", sixPM.displayHours());

        BerlinClock onePM = new BerlinClock("13:00:00");
        assertEquals("RROO\nRRRO", onePM.displayHours());
    }

    @Test
    public void minutesLampsLitRedForEveryQuarterHourPassed() {
        BerlinClock quarterPastMidnight = new BerlinClock("00:15:00");
        assertThat(quarterPastMidnight.displayMinutes(), containsString("ROOOOOOOO"));

        BerlinClock quarterToOne = new BerlinClock("00:45:00");
        assertThat(quarterToOne.displayMinutes(), containsString("YYRYYRYYR"));
    }

    @Test
    public void allMinuteLampsLitWell() {
        BerlinClock fiftyNineMinsPastMidnight = new BerlinClock("00:59:00");
        assertThat(fiftyNineMinsPastMidnight.displayMinutes(), is("YYRYYRYYRYY\nYYYY"));

        BerlinClock oneAM = new BerlinClock("01:00:00");
        assertThat(oneAM.displayMinutes(), is("OOOOOOOOOOO\nOOOO"));
    }

    @Test
    public void allLampsLitOrOffCorrectly() {
        BerlinClock testcase1 = new BerlinClock("13:17:01");
        assertThat(testcase1.displayTime(), is("O\nRROO\nRRRO\nYYROOOOOOOO\nYYOO"));

        BerlinClock aSecondBeforeMidnight = new BerlinClock("23:59:59");
        assertThat(aSecondBeforeMidnight.displayTime(), is("O\nRRRR\nRRRO\nYYRYYRYYRYY\nYYYY"));

        BerlinClock midnight = new BerlinClock("24:00:00");
        assertThat(midnight.displayTime(), is("Y\nRRRR\nRRRR\nOOOOOOOOOOO\nOOOO"));

        BerlinClock startOfNextDay = new BerlinClock("00:00:00");
        assertThat(startOfNextDay.displayTime(), is("Y\nOOOO\nOOOO\nOOOOOOOOOOO\nOOOO"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwExceptionForInvalidHours(){
        BerlinClock illegalTime = new BerlinClock("25:00:00");
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwExceptionForInvalidMiutes(){
        new BerlinClock("01:60:00");
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwExceptionForInvalidSeconds(){
        new BerlinClock("01:10:599");
    }
}
