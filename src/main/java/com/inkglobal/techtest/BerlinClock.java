package com.inkglobal.techtest;

public class BerlinClock extends Clock {

    private static final int TOTAL_FIVE_HOUR_LAMPS = 4;
    private static final int TOTAL_SINGLE_HOUR_LAMPS = 4;
    private static final int TOTAL_FIVE_MINUTE_LAMPS = 11;
    private static final int TOTAL_ONE_MINUTE_LAMPS = 4;

    private static final String LIT_QUARTER_HOUR_LAMP = "R";
    private static final String LIT_HOUR_LAMP = "R";
    private static final String LIT_MINUTE_LAMP = "Y";
    private static final String LIT_SECONDS_LAMP = "Y";

    private static final String TURN_OFF_LAMP = "O";


    public BerlinClock(String timeIn24hrFormat) {
        super(timeIn24hrFormat);
    }

    @Override
    protected String displayTime() {
        return displaySeconds() + timeSeparator() + displayHours() + timeSeparator() + displayMinutes();
    }

    @Override
    protected String displayHours() {
        return getFirstRowHourDisplay() + timeSeparator() + getSecondRowHourDisplay();
    }

    private String getFirstRowHourDisplay() {
        int numberOfFiveHourLampsToLit = getHours() / 5;
        return litLamps(numberOfFiveHourLampsToLit, TOTAL_FIVE_HOUR_LAMPS, LIT_HOUR_LAMP);
    }

    private String getSecondRowHourDisplay() {
        int numberOfSingleHourLampsToLit = getHours() % 5;
        return litLamps(numberOfSingleHourLampsToLit, TOTAL_SINGLE_HOUR_LAMPS, LIT_HOUR_LAMP);
    }

    private String litLamps(int numberOfLampsToLit, int totalLamps, String lampLitIndicator) {
        StringBuilder firstRowHourDisplay = new StringBuilder();
        for (int i = 1; i <= totalLamps; i++, numberOfLampsToLit--) {
            if(numberOfLampsToLit > 0) firstRowHourDisplay.append(lampLitIndicator);
            else firstRowHourDisplay.append(TURN_OFF_LAMP);
        }
        return firstRowHourDisplay.toString();
    }

    @Override
    protected String displayMinutes() {
        return getFirstRowMinutesDisplay() + timeSeparator() + getSecondRowMinutesDisplay();
    }

    private String getFirstRowMinutesDisplay() {
        StringBuilder minutesDisplay = new StringBuilder();
        int numberOfFiveMinuteLampsToLit =  getMinutes() / 5;
        for(int i = 1; i <= TOTAL_FIVE_MINUTE_LAMPS; i++, numberOfFiveMinuteLampsToLit--) {
            if(numberOfFiveMinuteLampsToLit > 0) {
                minutesDisplay.append((isQuarterHourLamp(i) ? LIT_QUARTER_HOUR_LAMP : LIT_MINUTE_LAMP));
            }
            else minutesDisplay.append(TURN_OFF_LAMP);
        }
        return minutesDisplay.toString();
    }

    private String getSecondRowMinutesDisplay() {
        int numberOfOneMinuteLampsToLit =  getMinutes() % 5;
        return litLamps(numberOfOneMinuteLampsToLit, TOTAL_ONE_MINUTE_LAMPS, LIT_MINUTE_LAMP);
    }

    private boolean isQuarterHourLamp(int minuteLampNumber) {
        return minuteLampNumber > 0 && minuteLampNumber % 3 == 0;
    }

    @Override
    protected String displaySeconds() {
        return getSeconds() % 2 == 0 ? LIT_SECONDS_LAMP : TURN_OFF_LAMP;
    }

    @Override
    protected String timeSeparator() {
        return System.lineSeparator();
    }
}
