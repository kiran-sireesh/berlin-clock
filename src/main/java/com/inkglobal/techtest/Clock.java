package com.inkglobal.techtest;

import java.util.regex.Pattern;

public abstract class Clock {

    private final String timeIn24hrFormat;
    private int hours, minutes, seconds;
    private Pattern timePattern = Pattern.compile("^([01]\\d|2[0-3]):([0-5]\\d):([0-5]\\d)$");

    public Clock(String timeIn24hrFormat) {
        this.timeIn24hrFormat = timeIn24hrFormat;
        parseTime();
    }

    private void parseTime() {
        boolean isExpectedTimeFormat = timePattern.matcher(timeIn24hrFormat).matches();
        if(!isExpectedTimeFormat)
            throw new IllegalArgumentException("Invalid time :" + timeIn24hrFormat);

        String[] timeDetails = timeIn24hrFormat.split(":");
        hours = Integer.parseInt(timeDetails[0]);
        minutes = Integer.parseInt(timeDetails[1]);
        seconds = Integer.parseInt(timeDetails[2]);
    }

    protected abstract String displayTime();

    protected abstract String displayHours();

    protected abstract String displayMinutes();

    protected abstract String displaySeconds();

    protected abstract String timeSeparator();

    public int getHours() {
        return hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public int getSeconds() {
        return seconds;
    }
}
